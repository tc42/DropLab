var CustomEvent = require('./custom_event_polyfill');
var Hook = require('./hook');

var HookInput = function(trigger, list) {
  Hook.call(this, trigger, list);
  this.type = 'input';
  this.event = 'input';
  this.addEvents();
};

Object.assign(HookInput.prototype, {
  addEvents: function(){
    var self = this;

    function mousedown(e) {
      var mouseEvent = new CustomEvent('mousedown.dl', {
        detail: {
          hook: self,
          text: e.target.value,
        },
        bubbles: true,
        cancelable: true
      });
      e.target.dispatchEvent(mouseEvent);
    }

    function input(e) {
      var inputEvent = new CustomEvent('input.dl', {
        detail: {
          hook: self,
          text: e.target.value,
        },
        bubbles: true,
        cancelable: true
      });
      e.target.dispatchEvent(inputEvent);
      self.list.show();
    }

    function keyup(e) {
      keyEvent(e, 'keyup.dl');
    }

    function keydown(e) {
      keyEvent(e, 'keydown.dl');
    }

    function keyEvent(e, keyEventName){
      var keyEvent = new CustomEvent(keyEventName, {
        detail: {
          hook: self,
          text: e.target.value,
          which: e.which,
          key: e.key,
        },
        bubbles: true,
        cancelable: true
      });
      e.target.dispatchEvent(keyEvent);
      self.list.show();
    }

    this.events = this.events || {};
    this.events.mousedown = mousedown;
    this.events.input = input;
    this.events.keyup = keyup;
    this.events.keydown = keydown;
    this.trigger.addEventListener('mousedown', mousedown);
    this.trigger.addEventListener('input', input);
    this.trigger.addEventListener('keyup', keyup);
    this.trigger.addEventListener('keydown', keydown);
  },
});

module.exports = HookInput;
