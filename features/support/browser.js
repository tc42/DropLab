var config = require('./config.json');
var jsdom = require('jsdom');
var fs = require('fs');
var path = require('path');

var wrapInCatch = function(source) {
  return [
    'try {',
    source,
    '} catch(e) { console.error(e.message, e.stack); throw e; }',
  ].join("\n");
}

var datasetPolyfill = wrapInCatch(fs.readFileSync(path.resolve(__dirname, './jsdom_dataset_polyfill.js')));
var droplabSrc = wrapInCatch(fs.readFileSync(path.resolve(__dirname, '../../dist/droplab.js')));
var sinonSrc = fs.readFileSync(path.resolve(__dirname, '../../node_modules/sinon/pkg/sinon.js'));

var browser = {
  load: function(path, src) {

    return new Promise(function(resolve, reject) {
      var url = [config.HOST, ':', config.PORT, path].join('');
      var virtualConsole = jsdom.createVirtualConsole().sendTo(console);

      jsdom.env({
        url: url,
        src: [
          datasetPolyfill,
          droplabSrc,
          sinonSrc,
        ].concat(src || []),
        virtualConsole: virtualConsole,
        done: function(err, window) {
          if(err) reject(err.message, err.stack);
          resolve(window);
        },
      });

    });
  },
};
module.exports = browser;
